const key = "98f5e8f09881aaffbbcf64d9731cdebf";

const today = new Date();
function dateFunction(d) {
  let months = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
  ];
  let days = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi",
  ];

  let day = days[d.getDay()];
  let date = "0" + d.getDate();
  let month = months[d.getMonth()];
  let year = d.getFullYear();

  return `${day} ${date} ${month} ${year}`;
}

let apiCall = function (city) {
  let url =
    "https://api.openweathermap.org/data/2.5/weather?q=" +
    city +
    "&appid=" +
    key +
    "&units=metric&lang=fr";

  fetch(url)
    .then((response) =>
      response.json().then((data) => {
        if (data.cod === "404") {
          const error = document.querySelector(".error");
          error.textContent = "Ville non trouvée";
        } else {
          document.querySelector(".weather-icon").src =
            "http://openweathermap.org/img/w/" + data.weather[0].icon + ".png";

          document.querySelector("#city").innerHTML =
            "<i class='fas fa-city'></i>" + data.name + ", " + data.sys.country;

          document.querySelector("#date").innerText = dateFunction(today);

          document.querySelector("#temp").innerHTML =
            "<i class='fas fa-thermometer-half'></i>" +
            Math.round(data.main.temp) +
            "°C ";
          document.querySelector("#humidity").innerHTML =
            "<i class='fas fa-tint'></i>" + data.main.humidity + " %";
          document.querySelector("#wind").innerHTML =
            "<i class='fas fa-wind'></i><span></span>" +
            Math.round(data.wind.speed) +
            " km/h";

          document.querySelector("#description").innerHTML =
            "<i class='fas fa-clipboard'></i>" + data.weather[0].description;
        }
      })
    )
    .catch((err) => console.log("Erreur : " + err));
};

//EventListener on submit form
document.querySelector("form").addEventListener("submit", function (e) {
  e.preventDefault();
  let ville = document.querySelector("#inputCity").value;
  apiCall(ville);
});

//OnLoad
apiCall("Chambéry");
